# Structs

A `Struct` defines the structure of a data set.

## Usage

A structure can be created by extending the `Struct` class and passing the defining structure to 
the `super`:

```dart
class Post extends Struct {
  Post()
      : super({
          'title': Contains('hello'),
          'text': IsString(),
          'rating': [
            IsInt(),
            Min(0),
            Max(10),
          ],
          'email': IsString(),
          'site': IsString(),
          'createDate': IsDate(),
        });
}

void main() {
  final post = Post();

  try {
    final postData = post.validate({
      'title': 'Hello', // should not pass
      'text': 'this is a great post about hell world', // should not pass
      'rating': 11, // should not pass
      'email': 'google.com', // should not pass
      'site': 'googlecom', // should not pass
    });
    // postData now contains the valid data according to your structure.
    print('Validation succeeded: $postData');
  } catch (err) {
    // Validation failed. The error contains all the failed constraints as a list.
    print('Validation failed: $err');
  }
}
```

### Validation options

TODO: write them

## Validation errors

The `Struct.validate` method returns an list of `ValidationError` instances. Each error describes 
a property on the object that has failed one or more constraints. Each error will have a 
`constraints` Map, each pair in the map is a set of the constraint that failed and a 
describing message of the constraint.

If the `Post` is used for validation then it would throw the following errors:

```dart
[
  {
    'target': {/* The post object */},
    'property': 'title',
    'value': 'Hello',
    'constraints': {
      'Length': 'title must be longer than or equal to 10 characters',
    }
  },
  {
    'target': {/* The post object */},
    'property': 'text',
    'value': 'this is a great post about hell world',
    'constraints': {
      'Length': 'text must contain a hello string',
    }
  }
]
```