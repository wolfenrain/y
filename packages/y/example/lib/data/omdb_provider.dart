import 'dart:convert';
import 'dart:io';

import 'package:y/y.dart';

class OmdbProvider extends DataProvider {
  OmdbProvider() : client = HttpClient();

  final HttpClient client;

  final String host = 'www.omdbapi.com';

  String get apiKey => Platform.environment['API_KEY']!;

  @override
  Future<List<JSON>> by(String path) {
    return get('s=highland').then((r) => r['Search'].cast<JSON>());
  }

  @override
  Future<JSON> byId(Id id) {
    return get('i=${id.value}');
  }

  Future<JSON> get(String endpoint) async {
    final request = await client.getUrl(Uri.parse(
      'https://$host/?$endpoint&apikey=$apiKey',
    ));

    final response = await request.close();
    return jsonDecode(await response.transform(utf8.decoder).join());
  }
}
