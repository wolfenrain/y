import 'dart:collection';

import 'package:y/src/utils/get.dart';
import 'package:y/src/utils/set.dart';
import 'package:y/y.dart';

/// A [Struct] defines the structure of any given data set.
///
/// A structure can be created by extending the `Struct` class and passing the
/// defining structure to the `super`:
///
/// ```dart
/// class Post extends Struct {
///   Post()
///       : super({
///           'title': Contains('hello'),
///           'text': IsString(),
///           'rating': [
///             IsInt(),
///             Min(0),
///             Max(10),
///           ],
///           'email': IsString(),
///           'site': IsString(),
///           'createDate': IsDate(),
///         });
/// }
/// ```
abstract class Struct {
  Struct(Map<String, dynamic> structure)
      : structure = UnmodifiableMapView(structure) {
    _validStructure(structure);
  }

  /// Structure representation of this [Struct].
  final Map<String, dynamic> structure;

  /// Validate given data according to this [Struct].
  Map<String, dynamic> validate(Map<String, dynamic> data) {
    final errors = <ValidationError>[];
    final obj = _validate(data, _properties(structure), errors: errors);
    if (errors.isNotEmpty) {
      // TODO: proper class
      throw errors;
    }
    return obj;
  }

  /// Internal validation, will recursively go through the structure and
  /// use it to validate the given data.
  Map<String, dynamic> _validate(
    Map<String, dynamic> data,
    Map<String, List<StructType>> properties, {
    required List<ValidationError> errors,
  }) {
    final output = <String, dynamic>{};
    final props = properties.entries
        .where((element) => element.key.split('.').length <= 2);

    for (final prop in props) {
      final property = prop.key.split('[]').first;
      final value = get(data, property);
      final constraints = prop.value;

      /// If the property path contains a list tag but the value isn't a list
      /// then it is a invalid value.
      if (prop.key.split('[]').length > 1 && value is! List) {
        errors.add(ValidationError(
          target: data,
          property: property,
          value: value,
          constraints: {
            'IsList': '$property should be a list of objects',
          },
        ));
        continue;
      }

      if (value is List) {
        // All the child properties that each object in the list should be
        // validated against.
        final childProperties = {
          for (final p in properties.entries)
            if (p.key.startsWith('$property[].'))
              p.key.replaceFirst('$property[].', ''): p.value,
        };

        final validatedValue = value.map((e) {
          return _validate(e, childProperties, errors: errors);
        }).toList();

        set(output, property, validatedValue);
      } else {
        // If it contains an IsOptional and the value is null, no need to check.
        if (constraints.whereType<IsOptional>().isNotEmpty && value == null) {
          continue;
        }
        // Map of all the messages and names of invalid constraints.
        final constraintMessages = <String, String>{};

        // If the value is null, we don't have to validate it because it is
        // required.
        if (value == null) {
          constraintMessages['required'] = '$property is required';
        } else {
          final args = ValidationArguments(
            value: value,
            constraints: constraints,
            target: data,
            property: property,
          );

          for (final constraint in constraints) {
            // Skip if it is the optional one.
            if (constraint is IsOptional) {
              continue;
            }
            if (!constraint.validate(value, args)) {
              constraintMessages[constraint.name] = constraint.message(args);
            }
          }
        }

        // If we have invalid constraints, add an error otherwise set the value
        // to the output.
        if (constraintMessages.isNotEmpty) {
          errors.add(ValidationError(
            target: data,
            property: property,
            value: value,
            constraints: constraintMessages,
          ));
        } else {
          set(output, property, value);
        }
      }
    }

    return output;
  }

  /// Validate if given structure is correctly implemented.
  bool _validStructure(dynamic struct, [String path = '']) {
    if (path == '' && struct is! Map) {
      throw UnsupportedError('The initial structure has to be of the type Map');
    }

    if (struct is Map<String, dynamic>) {
      return struct.entries.every((e) {
        return _validStructure(
          e.value,
          '$path${path.isNotEmpty ? '.' : ''}${e.key}',
        );
      });
    } else if (struct is List<StructType>) {
      return struct.every((e) {
        return _validStructure(e, '$path[${struct.indexOf(e)}]');
      });
    } else if (struct is ListOf) {
      return _validStructure(struct.structure, '$path[]');
    } else if (struct is StructType) {
      return true;
    }
    throw Exception('Path "$path" is not a valid type definition');
  }

  /// Retrieve the full property paths and their [StructType]s.
  Map<String, List<StructType>> _properties(dynamic struct,
      [String path = '']) {
    final paths = <String, List<StructType>>{};
    if (struct is Map) {
      paths.addAll(struct.entries
          .map(
        (e) =>
            _properties(e.value, '$path${path.isNotEmpty ? '.' : ''}${e.key}'),
      )
          .fold({}, (p, e) => p..addAll(e)));
    } else if (struct is List<StructType>) {
      final mappedStructs = struct.map((e) => _properties(e, '$path'));
      for (final mappedStruct in mappedStructs) {
        for (final entry in mappedStruct.entries) {
          if (paths.containsKey(entry.key)) {
            paths[entry.key]?.addAll(entry.value);
          } else {
            paths[entry.key] = entry.value;
          }
        }
      }
    } else if (struct is ListOf) {
      paths.addAll(_properties(struct.structure, '$path[]'));
    } else if (struct is StructType) {
      paths[path] = [struct];
    } else {
      throw UnsupportedError('${struct.runtimeType} is not vald in a Struct');
    }
    return paths;
  }
}
