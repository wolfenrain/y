class CountedItem<T> {
  const CountedItem(this.count, this.item);

  final int count;

  final T item;
}
