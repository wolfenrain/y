import 'package:y/y.dart';

class Min extends StructType {
  Min(this.minimal);

  final num minimal;

  @override
  bool validate(value, ValidationArguments args) =>
      value is num && value >= minimal;

  @override
  String message(ValidationArguments args) =>
      '${args.property} must not be less than $minimal';
}
