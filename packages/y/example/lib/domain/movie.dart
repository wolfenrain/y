import 'package:example/data/omdb_provider.dart';
import 'package:y/y.dart';

class _MovieStructure extends EntityStructure {
  _MovieStructure()
      : super({
          'id': Id(),
          'title': IsString(),
        }, withId: false);

  @override
  MovieData data() => MovieData();
}

class MovieData with DataOf<_MovieStructure> {
  String get id => state('id');

  String get title => state('title');

  MovieData update(JSON data) => create(merge(data));
}

class MovieRepository extends Repository<_MovieStructure, MovieData> {
  MovieRepository() : super(_MovieStructure(), OmdbProvider());
}
