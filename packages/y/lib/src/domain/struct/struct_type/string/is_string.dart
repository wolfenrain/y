import 'package:y/y.dart';

class IsString extends StructType {
  @override
  bool validate(value, ValidationArguments args) => value is String;

  @override
  String message(ValidationArguments args) =>
      '${args.property} must be a string';
}
