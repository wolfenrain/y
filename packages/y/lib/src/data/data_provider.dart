import 'package:y/y.dart';

abstract class DataProvider {
  Future<JSON> byId(Id id);

  Future<List<JSON>> by(String path);
}
