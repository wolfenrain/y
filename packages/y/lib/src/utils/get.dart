import 'package:y/src/utils/string_to_path.dart';

T? get<T>(dynamic obj, String path) {
  if (obj is! Map && obj is! List) return obj;

  final fragments = stringToPath(path);
  var index = -1;

  while (obj != null && ++index < fragments.length) {
    final key = int.tryParse(fragments[index]) ?? fragments[index];
    if (obj is List && key is! int) {
      return null;
    }
    obj = obj[key];
  }

  return obj;
}
