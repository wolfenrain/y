import 'package:test/test.dart';
import 'package:y/y.dart';

class Address extends Struct {
  Address()
      : super({
          'street': [
            IsOptional(),
            IsString(),
          ],
          'city': IsString(),
        });
}

final throwsValidationError = throwsA(isA<List<ValidationError>>());

void main() {
  group('Struct', () {
    final address = Address();

    group('.validate', () {
      test('is valid', () {
        expect(address.validate({'city': 'Amsterdam'}), {'city': 'Amsterdam'});
      });

      test('is invalid', () {
        expect(() => address.validate({}), throwsValidationError);
      });
    });
  });
}
