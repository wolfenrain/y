import 'package:test/test.dart';
import 'package:y_http/src/http_provider.dart';

void main() {
  test('Partition', () {
    final numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    final partition = numbers.partition((it) => it % 2 == 0);

    expect(partition.key, [2, 4, 6, 8, 10]);
    expect(partition.value, [1, 3, 5, 7, 9]);
  });
}
