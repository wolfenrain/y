import 'package:y/src/utils/string_to_path.dart';

T set<T>(T obj, String path, dynamic value) {
  if (obj is! Map && obj is! List) return obj;

  final fragments = stringToPath(path);
  final last = fragments.removeLast();

  fragments.fold<dynamic>(obj, (a, c) {
    final i = fragments.indexOf(c);

    // Does the key exist and is its value an object? Follow that path.
    if (a[c] is Map || a[c] is List) {
      return a[c];
    }
    // No: create the key.
    // Is the next key a potential list-index?
    final key = int.tryParse(fragments[i + 1]);
    if (key != null) {
      a[c] = []; // Yes, create a new list
    } else {
      a[c] = {}; // No, create a new object
    }
    return a[c];
  })[last] = value;

  // Return object, allows for chaining.
  return obj;
}
