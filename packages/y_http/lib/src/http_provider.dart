import 'dart:async';
import 'dart:io';

import 'package:y/y.dart';
import 'package:dartlin/dartlin.dart';
import 'package:y_http/y_http.dart';

typedef HttpHandler = Future<dynamic> Function(HttpContext context);

extension Partition<T> on Iterable<T> {
  /// Splits the original list into pair of lists, where first list contains elements for which
  /// [predicate] yielded `true`, while the second list contains elements for which [predicate]
  /// yielded `false`.
  ///
  /// ```dart
  /// final numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  /// final partition = numbers.partition((it) => it % 2 == 0);
  ///
  /// print(partition.key); // [2, 4, 6, 8, 10]
  /// print(partition.value); // [1, 3, 5, 7, 9]
  /// ```
  Pair<Iterable<T>, Iterable<T>> partition(bool Function(T) predicate) {
    final first = <T>[];
    final second = <T>[];
    for (final i in this) {
      if (predicate(i)) {
        first.add(i);
      } else {
        second.add(i);
      }
    }
    return Pair(first, second);
  }
}

class HttpProvider extends AppProvider<HttpHandler, HttpContext> {
  @override
  void listen(int port, [String? message]) {
    HttpServer.bind(InternetAddress.anyIPv4, port).then((server) {
      server.listen((request) async {
        final context = HttpContext(request);

        final CountedItem<Route<HttpContext>> countedItem;
        try {
          countedItem = routes.firstWhere((r) => r.item.matches(context.path));
        } on StateError catch (_) {
          context.status(404).send('404 Not found');
          return;
        }
        final route = countedItem.item;
        final count = countedItem.count;
        context.params = route.params(context.path);

        final partition = handlers.partition((t) => t.count < count);
        final before = partition.key.map((t) => t.item);
        final after = partition.value.map((t) => t.item);

        try {
          try {
            for (final handler in before) {
              await handler(context);
              if (context.closed) {
                return;
              }
            }

            final result = await route.handler(context);
            if (result != null) {
              if (result is DataOf) {
                context.send(result.data);
              } else {
                context.send(result);
              }
            }

            for (final handler in after) {
              await handler(context);
              if (context.closed) {
                return;
              }
            }

            if (context.closed) {
              context.end();
            }
          } on List<ValidationError> catch (err) {
            throw BadRequestException(err);
          } catch (err, stacktrace) {
            print(err);
            print(stacktrace);
            throw InternalServerException(err);
          }
        } on AppException catch (err) {
          final statusCode = when(err, {
            isType<InternalServerException>(): () => 500,
          }).orElse(() => 500);
          final body = err.getBody();
          context.status(statusCode).json({
            if (body != null) ...body,
            'statusCode': statusCode,
            'message': err.message,
          });
        }
      });

      print(message);
    });
  }
}
