import 'package:example/domain/movie.dart';
import 'package:y/y.dart';
import 'package:y_http/y_http.dart';

class MovieResource extends Resource {
  MovieResource() : movieRepo = MovieRepository() {
    router('movies', [
      get('', search),
      post('', add),
      get(':id(tt[0-9]+)', byId),
    ]);
  }

  final MovieRepository movieRepo;

  Future<String> search(HttpContext context) async {
    return 'Hey?';
  }

  Future<void> add(HttpContext context) async {}

  Future<MovieData> byId(HttpContext context) async {
    final movie = await movieRepo.byId(Id(context.params['id']));

    movie.update({'x': 1});

    return movie.update({'title': 's'});
  }
}
