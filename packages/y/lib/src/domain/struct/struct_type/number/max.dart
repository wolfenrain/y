import 'package:y/y.dart';

class Max extends StructType {
  Max(this.maximal);

  final num maximal;

  @override
  bool validate(value, ValidationArguments args) =>
      value is num && value <= maximal;

  @override
  String message(ValidationArguments args) =>
      '${args.property} must not be greater than $maximal';
}
