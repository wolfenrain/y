import 'package:y/y.dart';

class ValidationError {
  const ValidationError({
    required this.target,
    required this.property,
    required this.value,
    required this.constraints,
  });

  /// The object on which the validation was done.
  final dynamic target;

  /// Property path of the invalid [value] in [target].
  final String property;

  /// Value that was invalid.
  final dynamic value;

  /// Map of constraints that considered [value] to be invalid.
  final Map<String, String> constraints;

  /// Convert to JSON object.
  JSON toJSON({bool withTarget = false}) {
    return {
      if (withTarget) 'target': target,
      'property': property,
      'value': value,
      'constraints': constraints,
    };
  }

  @override
  String toString() => 'ValidationError ${toJSON()}';
}
