# DTO

A **DTO** is an object that defines how data is send over the network. It is a readonly data 
structure and should never hold any form of stateful data.
