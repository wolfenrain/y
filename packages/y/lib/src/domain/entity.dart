import 'dart:collection';

import 'package:y/src/utils/get.dart';
import 'package:y/y.dart';

/// An [EntityStructure] represents a data structure for an entity.
abstract class EntityStructure extends Struct {
  EntityStructure(Map<String, dynamic> structure, {bool withId = true})
      : super({if (withId) 'id': IsInt(), ...structure});

  /// Return a new data instance for this [EntityStructure].
  DataOf<EntityStructure> data();
}

/// The [DataOf] represents the data of an [EntityStructure].
mixin DataOf<T extends EntityStructure> {
  /// The entity structure for the [data].
  late final T entity;

  /// The immutable data of this entity.
  late final Map<String, dynamic> data;

  // TODO: name this method
  T state<T>(String path) => get(data, path);

  /// Merge given data and validate it against the [EntityStructure].
  JSON merge(JSON data) => entity.validate({
        ...this.data,
        ...data,
      });

  /// Create a new data instance.
  U create<U extends DataOf<T>>(JSON data) => (entity.data() as U)
    ..entity = entity
    ..data = UnmodifiableMapView(data);
}
