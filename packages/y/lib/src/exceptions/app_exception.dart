import 'package:y/y.dart';

class AppException {
  AppException(this.message);

  final String message;

  JSON? getBody() {}
}
