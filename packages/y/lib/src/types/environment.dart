import 'dart:io';

class Environment {
  static Map<String, String?> _evironment = {
    ...Platform.environment,
  };

  static T? get<T>(String key) {
    final value = _evironment[key];
    if (value == null) return null;

    if (T == int) {
      return int.tryParse(value) as T?;
    }
    if (T == double) {
      return double.tryParse(value) as T?;
    }
    throw UnsupportedError('$T is not supported');
  }
}
