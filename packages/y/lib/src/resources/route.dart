import 'dart:async';

import 'package:y/y.dart';

final paramRegex = RegExp(
  r'(:(?<paramKey>[a-zA-Z0-9]+)(?:\((?<paramRegex>.*?)\)){0,1})',
);

typedef RouteHandler<U extends AppContext> = Future<dynamic> Function(
    U context);

class Route<U extends AppContext> {
  Route(this.method, this.originalPath, this.handler) {
    var path = originalPath;
    final matches = paramRegex.allMatches(originalPath);
    for (final match in matches) {
      final paramKey = match.namedGroup('paramKey');
      final paramRegex = match.namedGroup('paramRegex') ?? '.*?';
      path = path.replaceFirst(match.group(0)!, '(?<$paramKey>$paramRegex)');
    }
    pathRegex = RegExp('^/?$path/?\$');
  }

  final String method;

  final String originalPath;

  final RouteHandler<U> handler;

  late final RegExp pathRegex;

  bool matches(String path) => pathRegex.hasMatch(path);

  Map<String, String> params(String path) {
    return <String, String>{
      for (final m in pathRegex.allMatches(path))
        for (final n in m.groupNames) n: m.namedGroup(n)!
    };
  }

  Route<U> resolve({String prefix = ''}) =>
      Route<U>(method, '${prefix}/${originalPath}', handler);
}
