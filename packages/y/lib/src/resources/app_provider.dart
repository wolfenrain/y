import 'dart:collection';
import 'package:y/y.dart';

typedef Handler = Function;

abstract class AppProvider<H extends Handler, C extends AppContext> {
  final _handlers = <CountedItem<H>>[];
  final _routes = <CountedItem<Route<C>>>[];
  var _orderCount = 0;

  List<CountedItem<H>> get handlers => UnmodifiableListView(_handlers);
  List<CountedItem<Route<C>>> get routes => UnmodifiableListView(_routes);

  void use(H h) => _handlers.add(_item<H>(h));

  void route(Service s, Resource r) {
    r.routes.forEach((route) {
      final named = '${route.method} ${s.name} => ${route.originalPath}';
      if (route is! Route<C>) {
        return print('Skipping "$named" as it is not a $C route');
      }
      print(
        'Registering "${route.method} ${route.originalPath}" for Service ${s.name}',
      );
      _routes.add(_item(route));
    });
  }

  CountedItem<U> _item<U>(U item) => CountedItem<U>(_orderCount++, item);

  void listen(int port, [String? message]);
}
