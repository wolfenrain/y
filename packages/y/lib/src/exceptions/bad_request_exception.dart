import 'package:y/y.dart';

class BadRequestException extends AppException {
  BadRequestException(this.err) : super('Bad Request');

  final dynamic err;

  @override
  JSON? getBody() {
    if (err is List<ValidationError>) {
      return {
        'error': [
          for (final ValidationError error in err) error.toJSON(),
        ]
      };
    }
    return {'error': err.toString()};
  }
}
