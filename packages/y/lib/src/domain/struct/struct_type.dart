export './struct_type/number/is_double.dart';
export './struct_type/number/is_int.dart';
export './struct_type/number/max.dart';
export './struct_type/number/min.dart';
export './struct_type/string/contains.dart';
export './struct_type/string/is_string.dart';
export './struct_type/id.dart';
export './struct_type/is_date.dart';
export './struct_type/is_optional.dart';
export './struct_type/list_of.dart';

import 'package:y/y.dart';

abstract class StructType {
  /// Name of the [StructType].
  String get name => '$runtimeType';

  /// Validate if the given value is conform the [StructType].
  bool validate(value, ValidationArguments args);

  /// Message used for if the value was not according to the [StructType].
  String message(ValidationArguments args);
}
