import 'package:y/y.dart';

class InternalServerException extends AppException {
  InternalServerException(this.err) : super('Internal Server Exception');

  final dynamic err;

  @override
  JSON? getBody() {
    return {'error': err.toString()};
  }
}

void main() {
  Function.apply(x, [], {Symbol('x'): 1});
}

void x() {}
