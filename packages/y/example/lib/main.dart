import 'example_service.dart';
import 'resources/movie_resource.dart';

void main() {
  ExampleService.movie().uses([MovieResource()]).atPort(9001).start();
}
