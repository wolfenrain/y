import 'dart:collection';

import 'package:y/y.dart';

/// A Repository
class Repository<E extends EntityStructure, D extends DataOf<E>> {
  const Repository(this._entity, this._provider);

  /// The data provider for this [Repository].
  final DataProvider _provider;

  /// The entity structure for this [Repository].
  final E _entity;

  /// Retrieve an entity by given [id].
  Future<D> byId(Id id) {
    return _provider.byId(id).then(_dataOf);
  }

  /// Create a new data representation of [E].
  D _dataOf(JSON data) {
    return (_entity.data() as D)
      ..entity = _entity
      ..data = UnmodifiableMapView(_entity.validate(data));
  }
}
