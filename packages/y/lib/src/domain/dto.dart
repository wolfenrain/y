import 'package:y/src/utils/get.dart';
import 'package:y/y.dart';

/// A [DTO] is an object that defines how data is send over the network.
///
/// A [DTO] is a readonly data structure and should never hold any form of
/// stateful data.
abstract class DTO extends Struct {
  /// The given [data] will be validated according to the given [structure].
  DTO(JSON data, Map<String, dynamic> structure) : super(structure) {
    _data = validate(data);
  }

  /// Internal data storage.
  late final JSON _data;

  /// Retrieve a property by given [path] from the data storage.
  T property<T>(String path) => get(_data, path);

  @override
  String toString() => '$runtimeType: ${_data.toString()}';
}
