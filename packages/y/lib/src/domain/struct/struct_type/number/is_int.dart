import 'package:y/y.dart';

class IsInt extends StructType {
  @override
  bool validate(value, ValidationArguments args) => value is int;

  @override
  String message(ValidationArguments args) => '${args.property} must be an int';
}
