<h2 align="center">y</h2>

<p align="center">
    Why not build microserices in Dart?
</p>

<p align="center">
    <a href="https://pub.dev/packages/y"><img src="https://img.shields.io/pub/v/y?label=pub" alt="plugin version" /></a>
    <a href="https://gitlab.com/wolfenrain/y/-/commits/main"><img src="https://gitlab.com/wolfenrain/y/badges/main/coverage.svg" alt="coverage report" /></a>
    <a href="https://gitlab.com/wolfenrain/y/-/commits/main"><img src="https://gitlab.com/wolfenrain/y/badges/main/pipeline.svg" alt="pipeline status" /></a>
    <a href="https://gitlab.com/wolfenrain/y/-/blob/main/packages/y/pubspec.yaml"><img src="https://img.shields.io/librariesio/release/pub/y?label=dependencies" alt="dependencies" /></a>
</p>

---
