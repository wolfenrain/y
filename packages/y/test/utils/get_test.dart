import 'package:test/test.dart';
import 'package:y/src/utils/get.dart';

void main() {
  group('get', () {
    test('should get value by key in map', () {
      final map = {'a': 1};

      expect(get(map, 'a'), 1);
    });

    test('should get value by index in list', () {
      final list = [1];

      expect(get(list, '0'), 1);
    });

    test('should get deep value by key in map', () {
      final map = {
        'a': {'b': 1}
      };

      expect(get(map, 'a.b'), 1);
    });

    test('should get deep value by index in list', () {
      final map = [
        {'b': 1}
      ];

      expect(get(map, '[0].b'), 1);
    });

    test('should get deep value by index in list that is in a map', () {
      final map = {
        'a': [
          {'b': 1}
        ]
      };

      expect(get(map, 'a.[0].b'), 1);
    });
  });
}
