# Entity

An **Entity** exists out of an `EntityStructure` class and a data instance class that uses 
`DataOf`.
