import 'dart:collection';

import 'package:y/y.dart';

abstract class Resource {
  final _routes = <Route>[];

  List<Route> get routes => UnmodifiableListView(_routes);

  Resource router(String base, List<Route> routes) => Try.use(this)
      .accept(
        (t) => t._routes.addAll(routes.map((r) => r.resolve(prefix: base))),
      )
      .value;

  Route route<U extends AppContext>(
          String method, String url, RouteHandler<U> handler) =>
      Route<U>(method, url, handler);

  Route get<U extends AppContext>(String url, RouteHandler<U> handler) =>
      Route<U>('GET', url, handler);

  Route post<U extends AppContext>(String url, RouteHandler<U> handler) =>
      Route<U>('POST', url, handler);
}
