import 'package:y/y.dart';

class Contains extends StructType {
  Contains(this.seed);

  final String seed;

  @override
  bool validate(value, ValidationArguments args) =>
      value is String && value.contains(seed);

  @override
  String message(ValidationArguments args) =>
      '${args.property} must contain a $seed string';
}
