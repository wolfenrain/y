import 'package:y/y.dart';

class ValidationArguments {
  ValidationArguments({
    required this.value,
    required this.constraints,
    required this.target,
    required this.property,
  });

  final dynamic value;

  final List<StructType> constraints;

  final dynamic target;

  final String property;
}
