import 'package:y/y.dart';

class IsDate extends StructType {
  @override
  bool validate(value, ValidationArguments args) => value is DateTime;

  @override
  String message(ValidationArguments args) =>
      '${args.property} must be a DateTime';
}
