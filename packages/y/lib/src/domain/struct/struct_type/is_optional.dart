import 'package:y/y.dart';

class IsOptional extends StructType {
  @override
  bool validate(value, ValidationArguments args) =>
      throw UnimplementedError('Should never be called');

  @override
  String message(ValidationArguments args) =>
      throw UnimplementedError('Should never be called');
}
