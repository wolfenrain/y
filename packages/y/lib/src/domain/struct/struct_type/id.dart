import 'package:y/y.dart';

class Id extends StructType {
  Id([this.value]);

  final dynamic value;

  @override
  bool validate(value, ValidationArguments args) =>
      value is int || value is String;

  @override
  String message(ValidationArguments args) =>
      '${args.property} should be of the type Id';
}
