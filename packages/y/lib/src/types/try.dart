typedef Func<T, U> = T Function(U t);

abstract class Try<T> {
  const Try._();

  T get value;

  Object get error;

  bool get isValid;

  Try<U> map<U>(Func<U, T> f);

  Try<T> recover(Func<T, Exception> f);

  Try<T> recoverFrom<V extends Exception>(Func<T, Exception> f);

  Try<T> accept(Func<void, T> f);

  Try<T> filter(Func<bool, T> predicate);

  T or(T value);

  T? orElse(T? value);

  T orThrow(Exception error);

  static Try<T> use<T>(T value) => to(() => value);

  static Try<T> to<T>(T Function() func) {
    try {
      return _Success<T>(func());
    } catch (err) {
      return _Failure<T>(err);
    }
  }

  static Try<T> _withArgs<T>(Function func, args) {
    try {
      return _Success<T>(func.call(args));
    } catch (err) {
      return _Failure<T>(err);
    }
  }

  static Try<T> on<T>(Try<T> Function() func) {
    try {
      return func();
    } catch (err) {
      return _Failure<T>(err);
    }
  }
}

class _Success<T> extends Try<T> {
  const _Success(this.value) : super._();

  @override
  final T value;

  @override
  Object get error => throw Exception('No error found');

  @override
  bool get isValid => true;

  @override
  Try<U> map<U>(Func<U, T> f) => Try._withArgs<U>(f, value);

  @override
  Try<T> recover(Func<T, Exception> f) => this;

  @override
  Try<T> recoverFrom<V extends Exception>(Func<T, Exception> f) => this;

  @override
  Try<T> accept(Func<void, T> f) => Try.on(() {
        f(value);
        return this;
      });

  @override
  Try<T> filter(Func<bool, T> predicate) => Try.on(() {
        if (predicate(value)) {
          return this;
        }
        throw Exception('Applying filter(${predicate.toString()}) failed.');
      });

  @override
  T or(T value) => this.value;

  @override
  T? orElse(T? value) => this.value;

  @override
  T orThrow(Exception error) => value;
}

class _Failure<T> extends Try<T> {
  const _Failure(this.error) : super._();

  @override
  T get value => throw error;

  @override
  final Object error;

  @override
  bool get isValid => false;

  @override
  Try<U> map<U>(Func<U, T> f) => _Failure<U>(error);

  @override
  Try<T> recover(Func<T, Exception> f) => Try._withArgs<T>(f, error);

  @override
  Try<T> recoverFrom<V extends Exception>(Func<T, Exception> f) =>
      Try.on(() => error is V ? recover(f) : this);

  @override
  Try<T> accept(Func<void, T> f) => this;

  @override
  Try<T> filter(Func<bool, T> predicate) => this;

  @override
  T or(T value) => value;

  @override
  T? orElse(T? value) => value;

  @override
  T orThrow(Exception error) => throw error;
}
