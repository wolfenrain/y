import 'dart:convert';
import 'dart:io';

import 'package:y/y.dart';

class HttpContext extends AppContext {
  HttpContext(this.request) : response = request.response {
    response.done.then((_) => _closed = true, onError: (_) => _closed = true);
  }

  bool _closed = false;

  bool get closed => _closed;

  final HttpRequest request;

  final HttpResponse response;

  String get method => request.method;

  String get path => request.uri.path;

  late final Map<String, String> params;

  // Future<String> readAsString([Encoding encoding = utf8]) {
  //   final completer = Completer<String>();

  //   final buf = StringBuffer();
  //   request.transform(encoding.decoder).listen(buf.write)
  //     ..onError(completer.completeError)
  //     ..onDone(() => completer.complete(buf.toString()));

  //   return completer.future;
  // }

  HttpContext status(int statusCode) =>
      Try.use(this).accept((t) => t.response.statusCode = statusCode).value;

  HttpContext send(Object? value) => Try.use(this)
      .accept((t) => t.response.write(value))
      .accept((t) => t.end())
      .value;

  HttpContext end() => Try.use(this)
      .accept((t) => t._closed = true)
      .accept((t) => t.response.close())
      .value;

  HttpContext json(Object? value) => Try.use(this)
      .accept((t) => t.response.headers.set('Content-Type', 'application/json'))
      .accept((t) => t.response.write(jsonEncode(value)))
      .accept((t) => t.end())
      .value;
}
