import 'package:y/y.dart';

class IsDouble extends StructType {
  @override
  bool validate(value, ValidationArguments args) => value is double;

  @override
  String message(ValidationArguments args) =>
      '${args.property} must be an double';
}
