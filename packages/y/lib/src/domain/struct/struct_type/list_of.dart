import 'dart:collection';

import 'package:y/y.dart';

class ListOf {
  ListOf(Struct struct) : structure = struct.structure;

  ListOf.raw(Map<String, dynamic> structure)
      : structure = UnmodifiableMapView(structure);

  final Map<String, dynamic> structure;
}
