import 'package:y/y.dart';

class Service {
  Service(this.name, this.app);

  final String name;

  final AppProvider app;

  int _port = 8080;

  final List<Resource> _resources = [];

  Service uses(List<Resource> resources) =>
      Try.use(this).accept((t) => t._resources.addAll(resources)).value;

  List<Handler> pre() => [];

  List<Handler> post() => [];

  Service atPort(int port) => Try.use(this).accept((t) => t._port = port).value;

  void start() {
    Try.use(this)
        .accept((t) => t.pre().forEach((p) => t.app.use(p)))
        .accept((t) => t._resources.forEach((r) => t.app.route(t, r)))
        .accept((t) => t.post().forEach((p) => t.app.use(p)))
        .accept((t) => t.app.listen(t._port,
            'Service $name listening on port $_port with ${_resources.length} resources.'));
  }
}
